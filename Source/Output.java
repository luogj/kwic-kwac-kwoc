import java.util.List;

public class Output {

	private List<Title> titles;

	public void postResults(AlphabeticShifts alphaShifts) {
		getTitles(alphaShifts);
		postTitles();
	}

	private void postTitles() {
		if (titles.isEmpty()) {
			System.out.println("There are no results to show.");
		} else {
			System.out.println("These are your results:");
			for (Title title : titles) {
				postTitle(title);
			}
		}
	}

	private void postTitle(Title title) {
		String myTitle = "";

		for (String word : title.getWords()) {
			myTitle = myTitle + word;
			myTitle = myTitle + " ";
		}
		myTitle = myTitle.trim();

		System.out.println(myTitle);
	}

	private void getTitles(AlphabeticShifts alphaShifts) {
		titles = alphaShifts.getTitles();
	}

}
