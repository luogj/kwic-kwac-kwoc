import java.util.Scanner;


public class Input {

	private static Scanner scanner = new Scanner(System.in);
	
	public void getInput(Characters chars) {
		System.out.println("Please enter the titles, separated by hitting the \"Enter\" key.\n"
				+ "Use an empty line to end.");
		
		while (true) {
			String title = scanner.nextLine();
			
			if (title.isEmpty()) {
				break;
			}
			
			chars.setTitle(title);
		}
	}
	
}
