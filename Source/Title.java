import java.util.LinkedList;
import java.util.Queue;


public class Title {
	private Queue<String> words;
	
	public Title(String title) {
		words = new LinkedList<String>();
		
		String[] titleElements = title.split(" ");
		
		for (String element : titleElements) {
			words.add(element);
		}
	}

	public Title(Title title) {
		words = new LinkedList<String>();
		
		this.words.addAll(title.getWords());
	}

	public Title() {
		words = new LinkedList<String>();
	}

	public Title(Title workingTitle, Title buffer) {
		words = new LinkedList<String>();
		words.addAll(workingTitle.getWords());
		words.addAll(buffer.getWords());
	}

	public Queue<String> getWords() {
		return words;
	}

	public String pollWord() {
		return words.poll();
	}

	public void addWord(String word) {
		words.add(word);
	}

	public String peekWord() {
		return words.peek();
	}
	
}