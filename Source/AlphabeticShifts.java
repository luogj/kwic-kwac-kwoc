import java.util.Collections;
import java.util.List;
import java.util.Vector;


public class AlphabeticShifts {

	private static List<Title> titles;
	
	public void alpha(CircularShift cirShift) {		
		getTitles(cirShift);
		sortTitles();
	}

	private void sortTitles() {
		TitleComparator titleComparator = new TitleComparator();
		Collections.sort(titles, titleComparator);
	}

	private void getTitles(CircularShift cirShift) {
		titles = new Vector<Title>();
		titles = cirShift.getTitles();
	}

	public List<Title> getTitles() {
		return titles;
	}

}
