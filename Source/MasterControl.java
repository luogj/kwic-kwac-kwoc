
public class MasterControl {
	static Input input;
	static Output output;
	static Characters chars;
	static CircularShift cirShift;
	static AlphabeticShifts alphaShifts;
	
	public static void main(String[] args) {
		input = new Input();
		output = new Output();
		chars = new Characters();
		cirShift = new CircularShift();
		alphaShifts = new AlphabeticShifts();
		
		input.getInput(chars);
		cirShift.setup(chars);
		alphaShifts.alpha(cirShift);
		output.postResults(alphaShifts);
	}
}
