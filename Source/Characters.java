import java.util.Vector;


public class Characters {
	
	private static Vector<String> titleStore = new Vector<String>();
	
	public void setTitle(String title) {
		titleStore.add(title);
	}

	public Vector<String> getTitles() {
		return titleStore;
	}

}
