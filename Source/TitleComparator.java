import java.util.Comparator;


public class TitleComparator implements Comparator<Title> {

	@Override
	public int compare(Title arg0, Title arg1) {
		String title1 = arg0.getWords().toString();
		String title2 = arg1.getWords().toString();
		
		return title1.compareToIgnoreCase(title2);
	}

}
