import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;


public class CircularShift {
	private static Vector<String> ignoreList;
	private static Queue<Title> titles;
	private static FileReader wordsToIgnore;
	private static Vector<Title> shiftedLines;
	private static BufferedReader reader;
	
	public void setup(Characters chars) {
		ignoreList = new Vector<String>();
		titles = new LinkedList<Title>();
		shiftedLines = new Vector<Title>();
		
		//System.out.println(System.getProperty("user.dir"));
		try {
			wordsToIgnore = new FileReader("Words to Ignore.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getWordsToIgnore();
		
		getSplitTitles(chars);
		
		performShifts();
	}

	private void performShifts() {
		while (!titles.isEmpty()) {
			Title title = titles.poll();
			Vector<Title> shifted = performShift(title);
			shiftedLines.addAll(shifted);
		}
	}

	private Vector<Title> performShift(Title title) {
		Vector<Title> shifted = new Vector<Title>();
		
		Title workingTitle = new Title(title);
		
		Title buffer = new Title();
		
		String word;
		
		while ((word = workingTitle.peekWord()) != null) {
			if (!doIgnore(word)) {
				Title newTitle = new Title (workingTitle, buffer);
				shifted.add(newTitle);
			}
			buffer.addWord(word);
			workingTitle.pollWord();
		}
		
		return shifted;
	}

	private boolean doIgnore(String word) {
		if (ignoreList.contains(word.toLowerCase())) {
			return true;
		}
		else {
			return false;
		}
	}

	private void getSplitTitles(Characters chars) {
		Vector<String> vectorOfTitles = new Vector<String>();
		vectorOfTitles.addAll(chars.getTitles());
		
		for (String title : vectorOfTitles) {
			titles.add(new Title(title));
		}
	}

	private void getWordsToIgnore() {
		reader = new BufferedReader(wordsToIgnore);
		
		String line;
		
		try {
			while ((line = reader.readLine()) != null) {
				ignoreList.add(line);
			}
		} catch (IOException e) {
			System.out.println("The program failed to read the file \"Words to Ignore.txt\"");
		}
	}

	public Vector<Title> getTitles() {
		return shiftedLines;
	}

		
}
